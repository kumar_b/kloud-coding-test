INSTRUCTIONS

1. Download solution

2. Download the Nuget packages

3. Build solution

4. Execute the Utility Project

6. Execute the Unit Tests project

7. Execute the API Project, append /api/cars to the URL to see the returned json data

8. Execute API UnitTests project

Solution contains the following items

1) Kloud.Domain - Model classes Person,Cars,CarData

2) Kloud.Common - Interfaces . Uses Castle Windsor for DI

3) Kloud.Utility - A console application which writes the output to the console. Property injection concept has been used for IOC

4) Kloud.UnitTests - Unit testing for the application. Tests GetOwnersByCarBrand method

5) Kloud.API - Produces a replica service that returns the same results as the API provided

6) Kloud.API.UnitTests - Unit testing of the API. Tests whether the controller returns the expected output.
