﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kloud.Domain
{
    public class Car
    {
        public string Brand { get; set; }
        public string Colour { get; set; }
    }
}
