﻿using Kloud.Domain;
using System.Linq;

namespace Kloud.Common.Interfaces
{
    /// <summary>
    /// DataContext interfact
    /// </summary>
    public interface IDataContext
    {
        Person[] Persons { get; set; }
    }
}
