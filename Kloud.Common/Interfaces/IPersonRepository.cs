﻿using System.Collections.Generic;
using Kloud.Domain;
using System.Threading.Tasks;

namespace Kloud.Common.Interfaces
{
    /// <summary>
    /// General read methods to access the list of Persons.
    /// </summary>
    public interface IPersonRepository
    {

        Dictionary<string, List<string>> GetOwnersByCarBrand();
    }
}