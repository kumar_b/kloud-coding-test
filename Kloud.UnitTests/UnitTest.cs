﻿using System;
using Kloud.Domain;
using Kloud.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Newtonsoft.Json;
using Kloud.Utility;
using System.Collections.Generic;
using Kloud.Utility.Repositories;

namespace Kloud.UnitTests
{
    [TestClass]
    public class UnitTest
    {
        
        [TestMethod]
        public void GetOwnersByCarBrand_ShouldReturnExpectedResults()
        {
            
            Person[] owners = LocalRepository.Instance.GetOwners();
            var context = new TestDataContext
            {
                Persons = new Person[]
                {
                   new Person
                   {
                       Name = "Bradley",Cars = new Car[]
                       {
                           new Car
                           {
                               Brand="MG",Colour="Blue"
                           }
                       }
                   },
                   new Person
                   {
                       Name = "Demetrios",Cars = new Car[]
                       {
                           new Car
                           {
                               Brand="Toyota",Colour="Green"
                           },
                           new Car
                           {
                               Brand="Holden",Colour="Blue"
                           }
                       }
                   },
                   new Person
                   {
                       Name = "Brooke",Cars = new Car[]
                       {
                           new Car
                           {
                               Brand="Holden",Colour=""
                           }
                       }
                   },
                   new Person
                   {
                       Name = "Kristin",Cars = new Car[]
                       {
                           new Car
                           {
                               Brand="Toyota",Colour="Blue"
                           },
                           new Car
                           {
                               Brand="Mercedes",Colour="Green"
                           },
                           new Car
                           {
                               Brand="Mercedes",Colour="Yellow"
                           }
                       }
                   },
                   new Person
                   {
                       Name = "Andre",Cars = new Car[]
                       {
                           new Car
                           {
                               Brand="BMW",Colour="Green"
                           },
                           new Car
                           {
                               Brand="Holden",Colour="Black"
                           }
                       }
                   },
                    new Person
                   {
                       Cars = new Car[]
                       {
                           new Car
                           {
                               Brand="Mercedes",Colour="Blue"
                           }
                       }
                   },
                    new Person
                   {
                       Name= "",Cars = new Car[]
                       {
                           new Car
                           {
                               Brand="Mercedes",Colour="Red"
                           },
                           new Car
                           {
                               Brand="Mercedes",Colour="Blue"
                           }
                       }
                   },
                    new Person
                   {
                       Name= "Matilda",Cars = new Car[]
                       {
                            new Car
                           {
                               Brand="Holden"
                           },
                           new Car
                           {
                               Brand="BMW",Colour="Black"
                           }
                          
                       }
                   },
                       new Person
                   {
                       Name= "Iva",Cars = new Car[]
                       {
                            new Car
                           {
                               Brand="Toyota",Colour="Purple"
                           },
                           new Car
                           {
                               Brand="BMW",Colour="Black"
                           }

                       }
                   },
                       new Person
                   {
                       Cars = new Car[]
                       {
                            new Car
                           {
                               Brand="Toyota",Colour="Blue"
                           },
                           new Car
                           {
                               Brand="Mercedes",Colour="Blue"
                           }

                       }
                   }
                }
            };
            var respository = new PersonRepository() { Context = context };
            var items = respository.GetOwnersByCarBrand();
            Assert.IsTrue(items.Count == 5);
            Assert.IsTrue(items["BMW"].Count == 3);
            Assert.IsTrue(items["Mercedes"].Count == 1);
            Assert.IsTrue(items["Toyota"].Count == 3);
            Assert.IsTrue(items["MG"].Count == 1);
            Assert.IsTrue(items["Holden"].Count == 4);
            Assert.AreEqual(items["Holden"].JoinWith(","),"Matilda,Brooke,Andre,Demetrios");
            Assert.AreEqual(items["BMW"].JoinWith(","), "Matilda,Iva,Andre");
            Assert.AreEqual(items["Toyota"].JoinWith(","), "Kristin,Demetrios,Iva");
            Assert.AreEqual(items["MG"].JoinWith(","), "Bradley");
            Assert.AreEqual(items["Mercedes"].JoinWith(","), "Kristin");

        }
    }
    public class LocalRepository
    {
        LocalRepository()
        {
        }

        public static LocalRepository Instance = new LocalRepository();

        public Person[] GetOwners()
        {
            return @"[
  {
    ""name"": ""Bradley"",
    ""cars"": [
      {
        ""brand"": ""MG"",
        ""colour"": ""Blue""
      }
    ]
  },
  {
    ""name"": ""Demetrios"",
    ""cars"": [
      {
        ""brand"": ""Toyota"",
        ""colour"": ""Green""
      },
      {
        ""brand"": ""Holden"",
        ""colour"": ""Blue""
      }
    ]
  },
  {
    ""name"": ""Brooke"",
    ""cars"": [
      {
        ""brand"": ""Holden"",
        ""colour"": """"
      }
    ]
  },
  {
    ""name"": ""Kristin"",
    ""cars"": [
      {
        ""brand"": ""Toyota"",
        ""colour"": ""Blue""
      },
      {
        ""brand"": ""Mercedes"",
        ""colour"": ""Green""
      },
      {
        ""brand"": ""Mercedes"",
        ""colour"": ""Yellow""
      }
    ]
  },
  {
    ""name"": ""Andre"",
    ""cars"": [
      {
        ""brand"": ""BMW"",
        ""colour"": ""Green""
      },
      {
        ""brand"": ""Holden"",
        ""colour"": ""Black""
      }
    ]
  },
  {
    ""cars"": [
      {
        ""brand"": ""Mercedes"",
        ""colour"": ""Blue""
      }
    ]
  },
  {
    ""name"": """",
    ""cars"": [
      {
        ""brand"": ""Mercedes"",
        ""colour"": ""Red""
      },
      {
        ""brand"": ""Mercedes"",
        ""colour"": ""Blue""
      }
    ]
  },
  {
    ""name"": ""Matilda"",
    ""cars"": [
      {
        ""brand"": ""Holden""
      },
      {
        ""brand"": ""BMW"",
        ""colour"": ""Black""
      }
    ]
  },
  {
    ""name"": ""Iva"",
    ""cars"": [
      {
        ""brand"": ""Toyota"",
        ""colour"": ""Purple""
      }
    ]
  },
  {
    ""cars"": [
      {
        ""brand"": ""Toyota"",
        ""colour"": ""Blue""
      },
      {
        ""brand"": ""Mercedes"",
        ""colour"": ""Blue""
      }
    ]
  }
]".FromJson<Person[]>();
        }
    }
}
static class Extensions
{
    public static string JoinWith(this IEnumerable<string> items, string delimiter) => string.Join(",", items);

 }
