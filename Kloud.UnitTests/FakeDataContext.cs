﻿using Kloud.Common.Interfaces;
using System.Linq;
using Kloud.Domain;

namespace Kloud.UnitTests
{
    public class TestDataContext : IDataContext
    {
        public Person[]  Persons { get; set; }
    }
}
