﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Kloud.Common.Interfaces;
using Kloud.Domain;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace Kloud.API.Controllers
{
    public class CarsController : ApiController
    {
        private static IWindsorContainer di = new WindsorContainer();
        [HttpGet]
        public IHttpActionResult Cars()
        {

            di.Install(FromAssembly.This());
            var repository = di.Resolve<IPersonRepository>();
            var persons = repository.GetOwnersByCarBrand();
            return Ok(persons);
        }
    }
}
