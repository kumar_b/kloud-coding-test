﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kloud.API.Controllers;
using System.Web;
using Kloud.Domain;
using Kloud.Common;
using Kloud.Utility;
using Newtonsoft.Json;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections.Generic;

namespace Kloud.API.UnitTests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void GetOwnersByCarBrand_ShouldReturnExpectedResults()
        {
            string testData = GetTestData();
            var controller = new CarsController();
            IHttpActionResult actionResult = controller.Cars();
            var contentResult = actionResult as OkNegotiatedContentResult<Dictionary<string, List<string>>>;
            Assert.AreEqual(testData, contentResult.Content.ToJson());

        }

        public string GetTestData()
        {
            return @"{""BMW"":[""Matilda"",""Andre""],""Holden"":[""Matilda"",""Brooke"",""Andre"",""Demetrios""],""Mercedes"":[""Kristin""],""MG"":[""Bradley""],""Toyota"":[""Kristin"",""Demetrios"",""Iva""]}"
                    ;
        }

    }
}
