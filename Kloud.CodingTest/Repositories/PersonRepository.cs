﻿using Kloud.Common.Interfaces;
using Kloud.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Kloud.Utility.Repositories
{
    /// <summary>
    /// Implementation of the IPersonRepository interfact
    /// </summary>
    public class PersonRepository : IPersonRepository
    {
        public IDataContext Context { get; set; }

        public Dictionary<string, List<string>> GetOwnersByCarBrand()
        {
           
            var _items = Context.Persons.SelectMany(customer => customer.Cars.Select(car =>
            new
            {
                Brand = car.Brand ?? "",
                Owner = customer.Name,
                Color = car.Colour
            }));

            var query =
            from item in _items
            where (!String.IsNullOrEmpty(item.Owner)) // data contains cars with no owners so we don't include them in the result
            group item by item.Brand into newGroup
            orderby newGroup.Key
            select newGroup.OrderBy(x => x.Color).ToList().DistinctBy(y => y.Owner); // some owners own more than 1 car of the same brand so we don't include them in the result
            Dictionary<string, List<string>> dataDict = new Dictionary<string, List<string>>();
            
            foreach (var item in query)
            {
                foreach (var subitem in item)
                {

                    if (dataDict.ContainsKey(subitem.Brand))
                    {
                        dataDict[subitem.Brand].Add(subitem.Owner);
                    }
                    else
                    {
                        dataDict[subitem.Brand] = new List<string> { subitem.Owner };
                    }
                }
            }
            return dataDict;
        }
    }
}
