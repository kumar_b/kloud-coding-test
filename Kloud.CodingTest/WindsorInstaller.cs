﻿using Kloud.Common.Interfaces;
using Kloud.Utility.Data;
using Kloud.Utility.Repositories;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace Kloud.Utility
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            container.Register(Component.For<IPersonRepository>().ImplementedBy<PersonRepository>());
            container.Register(Component.For<IDataContext>().ImplementedBy<DataContext>());

        }
    }
}
