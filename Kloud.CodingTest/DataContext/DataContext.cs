﻿using Kloud.Common.Interfaces;
using Kloud.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kloud.Utility.Data
{
    public class DataContext : IDataContext
    {
        public Person[] Persons
        {
            get
            {
                return Task.Run(async () => await GetData()).Result;
            }

            set
            {
                throw new NotImplementedException();
            }

        }

        public async Task<Person[]> GetData()
        {
            var url = @"https://Kloudcodingtest.azurewebsites.net/api/cars";
            using (var client = new HttpClient())
            using (var response = await client.GetAsync(url))
            using (var content = response.Content)
            {
                string json = await content.ReadAsStringAsync();

                Person[] result = JsonConvert.DeserializeObject<Person[]>(json);

                return result;
            }
        }
    }
}
